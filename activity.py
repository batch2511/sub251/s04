from abc import ABC, abstractclassmethod


class Animal (ABC):
    @abstractclassmethod
    def eat(self, food):
        pass
    def make_sound(self):
        pass

        


class Dog(Animal):
    def __init__(self, name, breed, age):
        super().__init__()
        self._name = name
        self.Breed = breed
        self.age = age

    def get_name(self):
        return self._name

    def set_name(self, name):
        self._name = name

    def get_breed(self):
       return self._breed

    def set_breed(self, breed):
        self._breed = breed
    
    def eat(self, food):
        print(f"Eating {food} ")

    def make_sound(self):
        print(f"zzzzZZZZZ")
    
    def call(self):
        print(f"Hi I'm {self._name}")



dog1 = Dog("Whitey", "Buldog", 100)
dog1.eat("Dog Food")
dog1.make_sound()
dog1.call()

class Cat(Animal):
    def __init__(self, name, breed, age):
        super().__init__()
        self._name = name
        self.Breed = breed
        self.age = age

    def get_name(self):
        return self._name

    def set_name(self, name):
        self._name = name

    def get_breed(self):
       return self._breed

    def set_breed(self, breed):
        self._breed = breed
    
    def eat(self, food):
        print(f"Eating {food} ")

    def make_sound(self):
        print(f"brbrbrbrbrbr")
    
    def call(self):
        print(f"Hi I'm {self._name}")


cat1 = Dog("Muning", "Pusakal", 300)
cat1.eat("Cat Food")
cat1.make_sound()
cat1.call()